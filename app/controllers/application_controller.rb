class ApplicationController < ActionController::API
    def current_user
        token = request.headers["Authorization"]
        return nil unless token.present?
        decoded = JsonWebToken.decode(token)
        return nil unless decoded.present?
        User.find(decoded[0]["user_id"]) # [{user_id: 1}, blabla]
    end

    rescue_from CanCan::AccessDenied do |exception|
        render json: {message: "Permissao Negada, você não tem permissão a esse acesso"}, status: 403
    end
end
