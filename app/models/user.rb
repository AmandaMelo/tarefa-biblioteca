class User < ApplicationRecord
    has_secure_password

    #validações
    validates :name, :email, :telephone,:password_confirmation, presence: true 
    validates :email, :cpf, presence: true, uniqueness: true
    validates :email, format: {with: /\b[A-Z0-9._%a-z\-]+@id\.uff\.br\z/, message: "O email deve ser do domínio idUFF"}
    validates :cpf, format: {with: /\A([0-9]{3}+.){2}+[0-9]{3}+-+[0-9]{2}\Z/, message: "O CPF deve estar no formato 999.999.999-99"}

    #funções
    enum role: {client: 0, clerk: 1, admin: 2}

    after_create :createRole
    def createRole
        if self.client?
            userRole = Client.new(user_id: self.id)
        elsif self.clerk?
            userRole = Clerk.new(user_id: self.id)
        elsif self.admin?
            userRole = Admin.new(user_id: self.id)
        end
        userRole.save
    end
end
