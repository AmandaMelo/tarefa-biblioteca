class Author < ApplicationRecord

    #relações
    has_many :books

    #validações
    validates :name, presence: true, uniqueness: true
end
