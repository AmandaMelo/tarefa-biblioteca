class Book < ApplicationRecord

  #relações
  belongs_to :author
  has_many :loans

  #validações
  validates :url_image, :edition, :theme, :issue, :kind, presence: true
end
