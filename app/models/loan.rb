class Loan < ApplicationRecord
    #relações
    belongs_to :book
    belongs_to :user
  
    #validações
    validate :has_loan_open, on: [:create]
  
    #funções
    enum status: {open: 0, closed: 1}
  
    def has_loan_open
      alreadyLoan = Loan.find_by(user_id: self.user_id, status: "open")
      if alreadyLoan.present?
        self.errors.add(:has_loan_open, "Você já tem um empréstimo em aberto.")
      end
    end

    def late?
      expiry_date < Date.today
    end

    def calc_fine
      self.delay_days = Date.today - expiry_date
      self.fine = self.delay_days * 5
    end

end
