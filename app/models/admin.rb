class Admin < ApplicationRecord

  # relações
  belongs_to :user
  has_many :clerks
end
