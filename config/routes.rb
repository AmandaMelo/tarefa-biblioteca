Rails.application.routes.draw do
  resources :loans
  resources :authors
  resources :books
  post '/login', to: 'authentication#login'
  post '/sign_up', to: 'authentication#sign_up'
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
