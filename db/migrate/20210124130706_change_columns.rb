class ChangeColumns < ActiveRecord::Migration[5.2]
  def change
    remove_column :loans, :status
    add_column :loans, :status, :integer, default: 0

    remove_column :loans, :fine
    add_column :loans, :fine, :integer, default: 0

    remove_column :loans, :daley_days
    add_column :loans, :daley_days, :integer, default: 0
  end
end
