class ChangeColumn < ActiveRecord::Migration[5.2]
  def change
    remove_column :loans, :daley_days
    add_column :loans, :delay_days, :integer, default: 0
  end
end
