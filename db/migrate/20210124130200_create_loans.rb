class CreateLoans < ActiveRecord::Migration[5.2]
  def change
    create_table :loans do |t|
      t.date :expiry_date
      t.integer :daley_days
      t.integer :fine
      t.references :book_id, foreign_key: true
      t.references :user_id, foreign_key: true
      t.integer :status
      
      t.timestamps
    end
  end
end
