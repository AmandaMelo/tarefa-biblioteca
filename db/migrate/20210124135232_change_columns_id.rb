class ChangeColumnsId < ActiveRecord::Migration[5.2]
  def change
    remove_column :loans, :book_id_id
    add_column :loans, :book_id, :integer
    add_index :loans, :book_id, name: "index_loans_on_book_id"

    remove_column :loans, :user_id_id
    add_column :loans, :user_id, :integer
    add_index :loans, :user_id, name: "index_loans_on_user_id"
  end
end
